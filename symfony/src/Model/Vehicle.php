<?php


namespace App\Model;


use Doctrine\Common\Collections\ArrayCollection;

class Vehicle
{
    /** @var int */
    public $Count;

    /** @var ArrayCollection */
    public $Results;

    public function getCount(): int
    {
        return $this->Count;
    }

    public function setCount(int $Count): void
    {
        $this->Count = $Count;
    }

    public function getResults(): ArrayCollection
    {
        return $this->Results;
    }

    public function setResults($Results): void
    {
        $this->Results = $Results;
    }


}
