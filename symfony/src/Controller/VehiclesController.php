<?php

namespace App\Controller;

use App\Service\NhtsaService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VehiclesController extends AbstractFOSRestController
{
    private $nhtsaService;

    public function __construct(NhtsaService $nhtsaService)
    {
        $this->nhtsaService = $nhtsaService;
    }

    public function getVehicle(Request $request, int $year, string $manufacturer, string $model): Response
    {
        $rating = $request->query->has('withRating');

        return $this->json($this->getVehicleFromNhtsa($year, $manufacturer, $model, $rating));
    }

    public function searchVehicle(Request $request): Response
    {
        $year = $request->request->get('modelYear');
        $manufacturer = $request->request->get('manufacturer');
        $model = $request->request->get('model');

        if ($year === null || $manufacturer === null || $model === null) {
            return $this->json(['Count' => 0, 'Results' => []]);
        }

        return $this->json($this->getVehicleFromNhtsa($year, $manufacturer, $model));
    }

    private function getVehicleFromNhtsa(int $year, string $manufacturer, string $model, bool $rating = false): array
    {
        $vehiclesResponse = $this->nhtsaService->getVehicle($year, $manufacturer, $model);
        if ($rating === true) {
            $vehiclesResponse = $this->nhtsaService->getAllRatings($vehiclesResponse);
        }

        return $vehiclesResponse;
    }
}
