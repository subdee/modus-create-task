<?php


namespace App\Service;


use GuzzleHttp\Client;

class NhtsaService
{
    private $client;
    private $mapper;

    public function __construct(Client $client, NhtsaMapper $mapper)
    {
        $this->client = $client;
        $this->mapper = $mapper;
    }

    public function getVehicle(int $year, string $manufacturer, string $model): array
    {
        $result = $this->client->get("SafetyRatings/modelyear/{$year}/make/{$manufacturer}/model/{$model}?format=json");

        $content = (string)$result->getBody();

        return $this->mapper->mapVehicleResponse($content);
    }

    public function getVehicleRatings(int $vehicleId): array
    {
        $result = $this->client->get("SafetyRatings/VehicleId/{$vehicleId}?format=json");

        $rating = json_decode((string)$result->getBody(), true);
        if ($rating['Count'] === 0) {
            return [$vehicleId => 'Not Rated'];
        }

        return [$rating['Results'][0]['VehicleId'], $rating['Results'][0]['OverallRating']];
    }

    public function getAllRatings(array $vehiclesResponse): array
    {
        $ratings = [];

        foreach ($vehiclesResponse['Results'] as $vehicle) {
            $vehicleRatings = $this->getVehicleRatings($vehicle['VehicleId']);
            $ratings[$vehicleRatings[0]] = $vehicleRatings[1];
        }

        return $this->mapper->mapRatingsToVehicles($vehiclesResponse, $ratings);
    }
}
