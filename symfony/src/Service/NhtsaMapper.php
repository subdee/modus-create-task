<?php


namespace App\Service;


use App\Model\Vehicle;
use Symfony\Component\Serializer\SerializerInterface;

class NhtsaMapper
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function mapVehicleResponse(string $content): array
    {
        $content = $this->serializer->deserialize($content, Vehicle::class, 'json');
        $results = [];

        if ($content === null || $content->Count === 0) {
            return ['Count' => 0, 'Results' => $results];
        }

        foreach ($content->Results as $result) {
            $results[] = [
                'Description' => $result['VehicleDescription'] ?? '',
                'VehicleId' => $result['VehicleId'] ?? ''
            ];
        }

        return [
            'Count' => $content->Count,
            'Results' => $results
        ];
    }

    public function mapRatingsToVehicles(array $vehicles, array $ratings): array
    {
        foreach ($vehicles['Results'] as $index => $vehicle) {
            $vehicle['CrashRating'] = $ratings[$vehicle['VehicleId']];
            $vehicles['Results'][$index] = $vehicle;
        }

        return $vehicles;
    }
}
