modus-create-task API
=====================

### Requirements

- A computer connected to the interwebs 
- Docker 18.09
- Docker Compose 1.23

### Install

1. Clone this repository `git clone git@gitlab.com:subdee/modus-create-task.git`
2. Change into the project directory `cd modus-create-task`
3. Run docker-compose. Make sure the docker daemon is started. `docker-compose up -d`
4. Install dependencies `docker-compose exec php composer install`
5. ???
6. Profit

### Usage

Suggestion is to use [Postman](https://www.getpostman.com/) to call the API.
Base URL is `http://localhost:8080/`

### Common issues

#### Permissions / Cannot write to cache/log

If symfony complains that it cannot write to the cache or log directories, use the completely unsafe **chmod** command.

From the root folder: `chmod -R 777 logs var/logs` 
